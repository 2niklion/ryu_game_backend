exports.validObj = () => ({
    hasError: false,
    code: null,
    messages: [],
    data: null,
});