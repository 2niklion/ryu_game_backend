const responseMiddleware = (req, res, next) => {
    // TODO: Implement middleware that returns result of the query
    if (res.locals.valid.hasError) {
        let message = '';
        res.locals.valid.messages.forEach((msg) => (message += `${msg}; `));
        res.status(res.locals.valid.code).send({error: true, message: message});

        return next();
    }

    res.status(res.locals.valid.code).send(res.locals.valid.data);
    return next();
};

exports.responseMiddleware = responseMiddleware;
