const {user} = require('../models/user');
const {validObj} = require('../helpers/validObj');

const emailRegExp = /^[a-z0-9](\.?[a-z0-9]){1,}@gmail\.com$/gm;
const phoneRegExp = /^\+38(0\d{9})$/g;

const userValidation = (reqData, messages) => {
    for (const key of Object.keys(reqData)) {
        if (!user.hasOwnProperty(key)) {
            messages.push(`${key} can't be in body`);
        }

        if (key === 'id') {
            messages.push('id can\'t be in body');
            continue;
        }

        if (!reqData[key].length) {
            messages.push(`${key} is require`);
            continue;
        }

        if (key === 'email' && !emailRegExp.test(reqData.email.trim())) {
            messages.push('inappropriate email');
            continue;
        }

        if (key === 'phoneNumber' && !phoneRegExp.test(reqData.phoneNumber.trim())) {
            messages.push('phone number must be like +380XXXXXXXXXX');
            continue;
        }

        if (key === 'password' && reqData.password.length < 3) {
            messages.push('password must be bigger 3 char');
            continue;
        }
    }
};

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    const valid = validObj();

    if (!Object.keys(req.body).length) {
        valid.messages.push('body is empty');
    }

    for (const key of Object.keys(user)) {
        if (key === 'id') continue;
        if (!req.body.hasOwnProperty(key)) {
            valid.messages.push(`${key} is require`);
        }
    }

    userValidation(req.body, valid.messages);

    res.locals.valid = valid.messages.length ? {...valid, hasError: true, code: 400} : valid;

    return next();
};

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    const valid = validObj();

    if (!Object.keys(req.body).length) {
        valid.messages.push('body is empty');
    }

    userValidation(req.body, valid.messages);

    res.locals.valid = valid.messages.length ? {...valid, hasError: true, code: 400} : valid;

    return next();
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
