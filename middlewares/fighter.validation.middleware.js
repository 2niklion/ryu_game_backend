const {fighter} = require('../models/fighter');
const {validObj} = require('../helpers/validObj');

const isBetweenTwoNumber = (max, min, number) => {
    return number >= min && number <= max;
};

const fighterValidation = (reqData, messages) => {
    for (const key of Object.keys(reqData)) {
        if (!fighter.hasOwnProperty(key)) {
            messages.push(`${key} can't be in body`);
            continue;
        }

        if (key === 'id') {
            messages.push('id can\'t be in body');
            continue;
        }

        if (!reqData[key].toString().length) {
            messages.push(`${key} is require`);
            continue;
        }

        if (key === 'power' && (isNaN(reqData.power) || !isBetweenTwoNumber(100, 1, reqData.power))) {
            messages.push('power must be between 1 and 100');
            continue;
        }

        if (key === 'defense' && (isNaN(reqData.defense) || !isBetweenTwoNumber(10, 1, reqData.defense))) {
            messages.push('defense must be between 1 and 10');
            continue;
        }

        if (key === 'health' && (isNaN(reqData.health) || !isBetweenTwoNumber(120, 80, reqData.health))) {
            messages.push('health must be between 80 and 120');
            continue;
        }
    }
};

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    const valid = validObj();

    if (!Object.keys(req.body).length) {
        valid.messages.push('body is empty');
    }

    if (!req.body.hasOwnProperty('health')) {
        req.body.health = 100;
    }

    for (const key of Object.keys(fighter)) {
        if (key === 'id') continue;
        if (!req.body.hasOwnProperty(key)) {
            valid.messages.push(`${key} is require`);
        }
    }

    fighterValidation(req.body, valid.messages);

    res.locals.valid = valid.messages.length ? {...valid, hasError: true, code: 400} : valid;

    next();
};

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    const valid = validObj();

    if (!Object.keys(req.body).length) {
        valid.messages.push('body is empty');
    }

    fighterValidation(req.body, valid.messages);

    res.locals.valid = valid.messages.length ? {...valid, hasError: true, code: 400} : valid;

    next();
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;