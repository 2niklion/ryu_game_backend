const {Router} = require('express');
const UserService = require('../services/userService');
const {createUserValid, updateUserValid} = require('../middlewares/user.validation.middleware');
const {responseMiddleware} = require('../middlewares/response.middleware');

const router = Router();

// POST /api/users
router.post(
    '/',
    createUserValid,
    (req, res, next) => {
        if (res.locals.valid.hasError) return next();

        try {
            const newUser = UserService.createNewUser(req.body);

            res.locals.valid = {
                hasError: false,
                code: 200,
                data: newUser,
            };
        } catch (err) {
            res.locals.valid = {
                ...res.locals.valid,
                hasError: true,
                code: 400,
                messages: [err],
            };
        } finally {
            next();
        }

    },
    responseMiddleware
);

// GET /api/users
router.get(
    '/',
    (req, res, next) => {
        try {
            const allUsers = UserService.getAllUsers();

            res.locals.valid = {
                hasError: false,
                code: 200,
                data: allUsers
            };
        } catch (err) {
            res.locals.valid = {
                hasError: true,
                code: 404,
                messages: [err],
            };
        } finally {
            next();
        }

    },
    responseMiddleware);

// GET /api/users/:id
router.get(
    '/:id',
    (req, res, next) => {
        const {id} = req.params;

        try {
            const user = UserService.getUserById(id);

            res.locals.valid = {
                hasError: false,
                code: 200,
                data: user
            };
        } catch (err) {
            res.locals.valid = {
                hasError: true,
                code: 404,
                messages: [err],
            };
        } finally {
            next();
        }
    },
    responseMiddleware);

// PUT /api/users/:id
router.put(
    '/:id',
    updateUserValid,
    (req, res, next) => {
        if (res.locals.valid.hasError) return next();
        const {id} = req.params;

        try {
            const updatedUser = UserService.updateUserById(id, req.body);

            res.locals.valid = {
                hasError: false,
                code: 200,
                data: updatedUser
            };
        } catch (err) {
            res.locals.valid = {
                ...res.locals.valid,
                hasError: true,
                code: 404,
                messages: [err],
            };
        } finally {
            next();
        }
    },
    responseMiddleware);

// DELETE /api/users/:id
router.delete(
    '/:id',
    (req, res, next) => {
        const {id} = req.params;
        console.log(id);
        try {
            const deletedUser = UserService.deleteUserById(id);
            res.locals.valid = {
                hasError: false,
                code: 200,
                data: {message: 'user deleted', user: deletedUser}
            };
        } catch (err) {
            res.locals.valid = {
                hasError: true,
                code: 400,
                messages: [err],
            };
        } finally {
            next();
        }

    },
    responseMiddleware
);
// TODO: Implement route controllers for user

module.exports = router;
