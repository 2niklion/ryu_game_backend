const {Router} = require('express');
const FighterService = require('../services/fighterService');
const {responseMiddleware} = require('../middlewares/response.middleware');
const {createFighterValid, updateFighterValid} = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter
// GET /api/fighters
router.get(
    '/',
    (req, res, next) => {
        try {
            const allFighters = FighterService.getAllFighters();

            res.locals.valid = {
                hasError: false,
                code: 200,
                data: allFighters
            };
        } catch (err) {
            res.locals.valid = {
                hasError: true,
                code: 404,
                messages: [err]
            };
        } finally {
            next();
        }
    },
    responseMiddleware
);

// GET /api/fighters/:id
router.get(
    '/:id',
    (req, res, next) => {
        const {id} = req.params;
        try {
            const fighter = FighterService.getFighterById(id);

            res.locals.valid = {
                hasError: false,
                code: 200,
                data: fighter
            };
        } catch (err) {
            res.locals.valid = {
                hasError: true,
                code: 404,
                messages: [err]
            };
        } finally {
            next();
        }
    },
    responseMiddleware
);

// POST /api/fighters
router.post(
    '/',
    createFighterValid,
    (req, res, next) => {
        if (res.locals.valid.hasError) return next();

        try {
            const fighter = FighterService.createFighter(req.body);

            res.locals.valid = {
                hasError: false,
                code: 200,
                data: fighter
            };
        } catch (err) {
            res.locals.valid = {
                hasError: true,
                code: 400,
                messages: [err]
            };
        } finally {
            next();
        }
    },
    responseMiddleware
);

// PUT /api/fighters/:id
router.put(
    '/:id',
    updateFighterValid,
    (req, res, next) => {
        if (res.locals.valid.hasError) return next();
        const {id} = req.params;

        try {
            const updatedFighter = FighterService.updateFighter(id, req.body);

            res.locals.valid = {
                hasError: false,
                code: 200,
                data: updatedFighter
            };
        } catch (err) {
            res.locals.valid = {
                ...res.locals.valid,
                hasError: true,
                code: 404,
                messages: [err],
            };
        } finally {
            next();
        }
    },
    responseMiddleware
);

// DELETE /api/fighters/:id
router.delete(
    '/:id',
    (req, res, next) => {
        const {id} = req.params;

        try {
            const deletedFighter = FighterService.deleteFighterById(id);
            res.locals.valid = {
                hasError: false,
                code: 200,
                data: {message: 'user deleted', fighter: deletedFighter}
            };
        } catch (err) {
            res.locals.valid = {
                hasError: true,
                code: 400,
                messages: [err]
            };
        } finally {
            next();
        }
    },
    responseMiddleware
);

module.exports = router;