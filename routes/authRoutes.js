const {Router} = require('express');
const AuthService = require('../services/authService');
const {responseMiddleware} = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        // TODO: Implement login action (get the user if it exist with entered credentials)
        const userFromDB = AuthService.login({email: req.body.email});
        if (userFromDB.password !== req.body.password) {
            throw Error('Incorrect password');
        }

        res.locals.valid = {
            hasError: false,
            code: 200,
            data: {id: userFromDB.id, email: userFromDB.email}
        };
    } catch (err) {
        res.locals.valid = {
            hasError: true,
            code: 400,
            messages: [err]
        };
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;