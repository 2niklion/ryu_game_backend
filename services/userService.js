const {UserRepository} = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
    getAllUsers() {
        const users = UserRepository.getAll();
        if (!users.length) throw Error('list empty');
        return users;
    }

    getUserById(id) {
        const user = UserRepository.getOne({id});
        if (!user) throw Error('user not found');
        return user;
    }

    updateUserById(id, dataToUpdate) {
        const updatedUser = UserRepository.update(id, dataToUpdate);
        if (!updatedUser) throw Error('user didn\'t update');
        return updatedUser;
    }

    createNewUser(user) {
        const uniqueEmail = this.search({email: user.email});
        const uniquePhone = this.search({phoneNumber: user.phoneNumber});

        if (uniqueEmail || uniquePhone) throw Error('email of phone is busy');
        return UserRepository.create(user);
    }

    deleteUserById(id) {
        const deletedUser = UserRepository.delete(id);
        if (!deletedUser.length) throw Error('user didn\'t delete');
        return deletedUser;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();