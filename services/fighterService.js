const {FighterRepository} = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    getAllFighters() {
        const fighters = FighterRepository.getAll();
        if (!fighters.length) throw Error('list empty');
        return fighters;
    }

    getFighterById(id) {
        const fighter = FighterRepository.getOne({id});
        if (!fighter) throw Error('fighter not found');
        return fighter;
    }

    createFighter(fighter) {
        const nameUnique = this.search({name: fighter.name});
        if (nameUnique) throw Error('fighter exist');
        return FighterRepository.create(fighter);
    }

    updateFighter(id, dataToUpdate) {
        const updatedFighter = FighterRepository.update(id, dataToUpdate);
        if (!updatedFighter) throw Error('fighter didn\'t update');
        return updatedFighter;
    }

    deleteFighterById(id) {
        const deleteFighter = FighterRepository.delete(id);
        if (!deleteFighter.length) throw Error('fighter didn\'t delete');
        return deleteFighter;
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FighterService();